import React, {Component} from 'react';
import axios from '../../axios-pages'

class Page extends Component {
  state={
    title: '',
    content: '',
  };
  getData = () =>{
    axios.get(`pages/${this.props.match.params.name}.json`).then(response=>{
      this.setState({title: response.data.title, content: response.data.content})
    })
  };
  componentDidMount(){
    this.getData();
  }
  componentDidUpdate(prevProps){
    if (this.props.match.params.name !== prevProps.match.params.name){
      this.getData();
    }
  }
  render() {
    return (
      <div>
        <h1>{this.state.title}</h1>
        <p>{this.state.content}</p>
      </div>
    );
  }
}

export default Page;