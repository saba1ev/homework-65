import React, {Fragment} from 'react';
import Navigation from "../Navigation/Navigation";
import './Layout.css'

const Layout = ({children}) => {
  return (
    <Fragment>
      <Navigation/>
      <div>
        {children}
      </div>
    </Fragment>
  );
};

export default Layout;