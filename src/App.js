import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import Layout from "./component/Layout/Layout";
import Page from "./Container/Page/Page";
import EditPage from "./Container/EditPage/EditPage";

class App extends Component {

  render() {
    return (
      <Layout>
        <Switch>
          <Route path='/pages/admin' component={EditPage}/>
          <Route path='/pages/:name' component={Page}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
