import axios from 'axios';

const config = axios.create({
  baseURL: 'https://pages-sobolev.firebaseio.com/'
});

export default config