import React, {Component} from 'react';
import axios from '../../axios-pages';


class EditPage extends Component {
  state={
    title: '',
    content: '',
    category:''

  };

  selectChange = (event)=>{
    const category = event.target.value
    axios.get(`pages/${category}.json`).then(response=>{
      this.setState({title: response.data.title, content: response.data.content, category: category});
    })
  };
  saveData = () =>{
    const page = {
      title: this.state.title,
      content: this.state.content
    };
    axios.put(`pages/${this.state.category}.json`, page).then(()=>{
      this.props.history.push(`/pages/${this.state.category}`);
    })
  }
  changeHendler= (event)=>{
    this.setState({
      [event.target.name]: event.target.value
    })
  }
  render() {
    return (
      <div>
        <select onChange={(event)=>this.selectChange(event)} name="" id="">
          <option value="About">About</option>
          <option value="Home">Home</option>
          <option value="Contact">Contact</option>
          <option value="Divisions">Divisions</option>
        </select>
        <input value={this.state.title} onChange={(event)=>this.changeHendler(event)} type="text" name='title'/>
        <input value={this.state.content} onChange={(event)=>this.changeHendler(event)} type="text" name='content'/>
        <button onClick={this.saveData}>Add</button>
      </div>
    );
  }
}

export default EditPage;