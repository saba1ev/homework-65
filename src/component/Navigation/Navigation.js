import React from 'react';
import {NavLink} from 'react-router-dom';
import './Navigation.css'

const Navigation = () => {
  return (
    <ul className='NavigationItems'>
      <li className='NavigationItem'><NavLink to='/pages/Home'>Home</NavLink></li>
      <li className='NavigationItem'><NavLink to='/pages/About'>About</NavLink></li>
      <li className='NavigationItem'><NavLink to='/pages/Contact'>Contact</NavLink></li>
      <li className='NavigationItem'><NavLink to='/pages/Divisions'>Divisions</NavLink></li>
      <li className='NavigationItem'><NavLink to='/pages/admin'>Admin</NavLink></li>
    </ul>
  );
};

export default Navigation;